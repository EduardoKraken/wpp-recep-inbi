const whatsapp       = require("../../models/operaciones/whatsapp.model");
const fs             = require('fs');
const request        = require("request-promise-native");
const { v4: uuidv4 } = require('uuid')
const { exec }       = require('child_process');
const path           = require("path");
const ffmpegPath     = require('ffmpeg-static');
const ffmpeg         = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);

const config         = require('../../config/index');

// SOCKETS
const { Sockets } = require("../../clases/socketConection.js");
const socket = new Sockets();


// Validar que el estatus del whatsApp este activo
exports.verEstatusWhatsApp = async(req, res) => {
  try {
    
    const { from } = req.body

    const data = await socket.verEstatus( from ).then( response => response )

    if( !data ){
      return res.send({ message: 'La sesión está inactiva, favor de inicializarla' })
    }

    return res.send( data )

  } catch (error) {
    res.status(500).send( error )
  }
};

// Iniciar el whatsapp, generar QR y más
exports.inicializarWhatsApp = async(req, res) => {

  try {
    
    const { from } = req.body

    const data = await socket.inicializarWhatsApp( from ).then( response => response )

    return res.send({ message: 'Información retornada correctamente'})

  } catch (error) {
    res.status(500).send( error )
  }
};

// Destruir una sesión de whatsApp
exports.destroySesion = async(req, res) => {
  try {
    
    const { from } = req.body

    const data = await socket.destroySesion( from ).then( response => response )

    return res.send({ message: 'Información retornada correctamente'})

  } catch (error) {
    return res.status(500).send( error )
  }
};

// Obtener todos los chats del whatsapp
exports.whatChats = async(req, res) => {
  try {
    
    // Desestructuramos el body y obtenemos el from para saber quien manda el mensaje
    const { from } = req.body

    // Mandamos a obtener todos los contactos
    const chats = await socket.getContacts( from ).then( response => response )

    // Retornamos que todo esta bien 
    return res.send( chats )

  } catch (error) {
    console.log( error )
    return res.status(500).send( error )
  }
};


exports.enviarMensajeWpp = async(req, res) => {
  try {
    
    const { _serialized, mensaje, imagen, froms, audio, rutaAudio } = req.body


    let outputFilename;
    let ruta;

    if( parseInt(imagen) == 1 && parseInt( audio ) == 0 ){
      if( !req.files ){
        return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' })
      }

      // desestrucutramos los arvhios cargados
      const { file } = req.files

      const archivoGuardado = await guardarImagenes( file ).then( response => response )

      outputFilename = archivoGuardado.outputFilename
      ruta           = archivoGuardado.ruta
    }

    if( parseInt(audio) == 1 ){

      outputFilename = rutaAudio

      ruta = './../../whatsapp-imagenes/' + rutaAudio
    }

    // ENVIAR EL MENSAJE CON TODO Y ARCHIVO
    const enviarMensaje = await config.WHATSAPP.enviarMensaje( froms, _serialized, mensaje, imagen, outputFilename, audio ).then( response => response )

    // eliminar la imagen 
    if( parseInt( imagen ) == 1 && outputFilename ){
      // Eliminarrrr
      const archivoEliminado = await eliminarImagen( ruta ).then( response => response )
    }

    // Mandar a llamar al socket de que se envío un mensaje
    // socket.sendMessage( )

    res.send({ message: 'pjpok', enviarMensaje, outputFilename })

  } catch (error) {
    res.status(500).send({message:error ? error.message : 'Error en el servidor' })
  }
};

// exports.generarReciboWhatsApp = async(req, res) => {
//   try {
    
//     const { tos, mensaje, imagen, url, from, id_alumno, id_grupo, id_usuario, escuela, actualizar, telefono } = req.body

//     if( actualizar ){
//       // Actualizar numero de teléfono en escuela
//       const updateTelefonoERP = await whatsapp.updateTelefonoERP( id_alumno, telefono ).then( response => response )

//       // Actualizar telefono en erp viejito
//       const updateTelefonoLMS = await whatsapp.updateTelefonoLMS( id_alumno, tos, escuela ).then( response => response )
//     }

//     // CONSTRUIR EL NOMBRE DEL ARCHIVO
//     let outputFilename = uuidv4() + '.pdf'

//     // GENERAR EL ARCHIVO PDF ( DESCARGANDO... )
//     let pdfBuffer = await request.get({uri: url, encoding: null});

//     // GUARDANDO EL ARCHIVO EN EL SERVIDOR
//     let archivo = fs.writeFileSync('./../../whatsapp-imagenes/' + outputFilename, pdfBuffer);

//     // ENVIAR EL MENSAJE CON TODO Y ARCHIVO
//     const enviarRecioWha = await config.WHATSAPP.enviarMensajeInbi( tos, mensaje, imagen, outputFilename, from ).then( response => response )

//     const guardarInfoReciboPago = await whatsapp.guardarInfoReciboPago( id_alumno, id_grupo, id_usuario ).then( response => response )

//     // const { tos, mensaje, from, contacto } = req.body

//     // // ENVIAR EL MENSAJE CON TODO Y ARCHIVO
//     // const enviarRecioWha = await config.WHATSAPP.enviarMensajeInbi( tos, mensaje, from, contacto ).then( response => response )

//     res.send({ message: 'pjpok', enviarRecioWha })

//   } catch (error) {
//     console.log( error )
//     res.status(500).send({message: error ? error.message : 'Error en el servidor', error })
//   }
// };

// exports.enviarReciboErp = async(req, res) => {
//   try {
    
//     const { id_usuario, id_pago } = req.body

//     // consultar el telefono de la vendedora
//     const datosVendedora  = await whatsapp.datosWhatsVendedora( id_usuario ).then( response => response )
    
//     // CONSULTAR PAGO 
//     const datosReciboPago = await whatsapp.datosReciboPago( id_pago ).then( response => response )

//     if( !datosReciboPago ){ return res.status( 400 ).send({ message: 'El pago no existe, favor de validarlo con sistemas' })}

//     // Consultar telefono del alumno
//     const datosAlumno     = await whatsapp.datosWhatsAlumno( datosReciboPago.id_alumno ).then( response => response )

//     res.send({ datosVendedora, datosAlumno, datosReciboPago })

//   } catch (error) {
//     res.status(500).send({message:error ? error.message : 'Error en el servidor' })
//   }
// };


// guardarImagenes = async ( file, nombre ) => {
//   return new Promise((resolve, reject) =>{

//     const nombreSplit = file.name.split('.')
      
//     // Obtener la extensión del archivo 
//     const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
//     // modificar el nombre del archivo con un uuid unico
//     let outputFilename = uuidv4() + '.' + extensioArchivo

//     // extenciones validas
//     let extensiones = ['mp4', 'MOV', 'AVI', 'WMV', 'mp3', 'wav', 'wma', 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'pdf', 'audio/webm', 'webm', 'audio', 'mpeg' ]
//     ruta        = './../../whatsapp-imagenes/' + outputFilename

//     // validar que la extensión del archivo recibido sea valido
//     if( !extensiones.includes( extensioArchivo) )
//       return reject({message: `Hola, la estensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
  
//     file.mv(`${ruta}`, err => {
//       if( err )
//         return reject({ message: err })
//       else
//         resolve({ outputFilename, ruta })
//     })
//   })
// }

// eliminarImagen = async ( ruta ) => {
//   return new Promise((resolve, reject) =>{
//     fs.unlink(ruta, (error) => {
//       if (error) {
//         return reject({ message: err })
//       }
//       return resolve({ ruta })
//     });

//   })
// }


exports.parseAudio = (req, res) => {

  let BASE_IMAGE_PATH = path.resolve(__dirname, "..", "..", "whatsapp-imagenes");


  const uidFile = uuidv4()
  const nombreFileMpeg = uidFile + '.mpeg'
  const nombreFileMp3  = uidFile + '.mp3'


  const fullPathMpeg     = path.join(
    path.relative(__dirname, BASE_IMAGE_PATH),
    nombreFileMpeg
  );

  const fullPathMp3     = path.join(
    path.relative(__dirname, BASE_IMAGE_PATH),
    nombreFileMp3
  );

  console.log( req.body )

  fs.writeFile(fullPathMpeg, req.body, error => {
    if (error) {
      res.status(500).send({ message: error ? error.message : 'Error en el servidor'});
    } else {

      // ruta al archivo de entrada MPEG
      const inputFile = fullPathMpeg;

      // ruta al archivo de salida MP3
      const outputFile = fullPathMp3


      const command = ffmpeg(inputFile)
        .setFfmpegPath(ffmpegPath)
        .noVideo()
        .audioCodec('libmp3lame')
        .output(outputFile);

      // iniciar la conversión
      command.on('end', function() {
        console.log('Finished processing');
        // console.log(`File saved to ${fullPathMpeg}`);
        res.send( nombreFileMp3 );

      }).run()

    }
  });

};


exports.buscarWhatsAppTelefono = async(req, res) => {
  try {
    
    const { whaid, escuela, id_whatsapp } = req.body


    const usuariosWhatsApp = await whatsapp.usuariosWhatsApp( ).then( response => response )    

    const telefonos = usuariosWhatsApp.filter( el => { return el.escuela == escuela }).map(({ whatsappservidor }) => { return whatsappservidor })


    // Obtener si tiene alguna asignación con alguna vendedora o recepcionista:
    const getAsignacion = await whatsapp.getAsignacion( id_whatsapp ).then( response => response )    

    // Si existe alguna asignación, hay que indicarle quién es
    if( getAsignacion ){

      // buscamos si existe la vendedora
      const existeVendedora =  usuariosWhatsApp.find( el => el.id_usuario == getAsignacion.id_vendedora )

      // Si existe, la agregamos, si no, null
      getAsignacion['vendedora'] = existeVendedora ? existeVendedora.nombre_completo : null
    }
    
    // Buscar los mensajes para mostrar y que vengan de parte de esos teléfonos, osea, inbi o fast
    let mensajesWha = await whatsapp.getMensajesIdWhat( whaid, telefonos ).then( response => response )

    mensajesWha = mensajesWha.sort((a, b) => a.idmensajes_whatsapp - b.idmensajes_whatsapp)

    if( !mensajesWha.length ){ return res.status( 400 ).send({ message: 'Sin mensajes' })}

    const vendedoras   = await whatsapp.usuariosWhatsApp( ).then( response => response ) 

    for( const i in mensajesWha ){
      const { mio, tos, froms } = mensajesWha[i]

      const existeVendedora1 = vendedoras.find( el => el.whatsappservidor == tos )
      const existeVendedora2 = vendedoras.find( el => el.whatsappservidor == froms )

      if( existeVendedora1 ){
        mensajesWha[i].url_servidor = existeVendedora1.url_servidor
        mensajesWha[i].vendedora    = existeVendedora1.nombre_completo
      }else if(existeVendedora2){
        mensajesWha[i].url_servidor = existeVendedora2.url_servidor
        mensajesWha[i].vendedora    = existeVendedora2.nombre_completo
      }else{
        mensajesWha[i].url_servidor = ''
        mensajesWha[i].vendedora    = ''
      }
    }

    return res.send({ mensajesWha, getAsignacion })

  } catch (error) {
    return res.status(500).send({message : error ? error.message : 'Error en el servidor '})
  }
};