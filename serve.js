// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var   cors       = require("cors");
const fileUpload = require("express-fileupload");
const fs         = require("fs");
// IMPORTAR EXPRESS
const app        = express();
const config     = require('./config/index');

//Para servidor con ssl
const server     = require('http').createServer(app);

// Rutas estaticas
app.use('/whatsapp-imagenes',             express.static('./../../whatsapp-imagenes'));

// app.use(session({
//   store: MongoStore.create({ mongoUrl: 'mongodb://localhost/mydatabase' })
// }));

// IMPORTAR PERMISOS
app.use(cors({ origin: '*' }));
// parse requests of content-type: application/json
app.use(bodyParser.json({limit: '5000mb'}));
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload()); //subir archivos
app.use(bodyParser.raw({ type: 'audio/mpeg', limit: '50mb' }));


// ********************    SOCKET        ***************** //

// Desestructuramos el socket 
const { Sockets } = require("./clases/socketConection.js");

// Creamos una instancia del socket
const socket = new Sockets();

// GUARDAMOS LA VARIABLE 
config.SOCKET = socket

// Inicilizar el socket
socket.generarConexion( server );

app.socketIO = socket

// ********************    SOCKET        ***************** //


// ********************    WHATSAPP        ***************** //

// INBI
// config.WHATSAPP = require("./helpers/whatsEncargadasInbi.js");
// config.WHATSAPP = require("./helpers/whatsServidor1.js");

// FAST
// config.WHATSAPP = require("./helpers/whatsEncargadaFast.js");
// config.WHATSAPP = require("./helpers/whatsServidor2.js");

const archivoWhatsApp = process.env.WHATSAPP

if( archivoWhatsApp == 1 ){
  // SISTEMAS
  config.WHATSAPP =  require("./helpers/sistemas.js");
}

if( archivoWhatsApp == 2 ){
  config.WHATSAPP = require("./helpers/whatsEncargadasInbi.js");
}

if( archivoWhatsApp == 3 ){
  config.WHATSAPP = require("./helpers/whatsServidor1.js");
}

if( archivoWhatsApp == 4 ){
  config.WHATSAPP = require("./helpers/whatsEncargadaFast.js");
}

if( archivoWhatsApp == 5 ){
  config.WHATSAPP = require("./helpers/whatsServidor2.js");
}

if( archivoWhatsApp == 6 ){
  config.WHATSAPP = require("./helpers/reclutadoras.js");
}


// ********************    WHATSAPP        ***************** //


// ----IMPORTAR RUTAS---------------------------------------->
require("./routes/whatsapp/whatsapp.routes")(app)

/// SERVER LISTEN
server.listen(config.PORT, () => {
    console.log(`Servidor escuchando en el puerto ${config.PORT}`);
});

