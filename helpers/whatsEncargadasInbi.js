const { Client, LegacySessionAuth, LocalAuth, MessageMedia, RemoteAuth  } = require('whatsapp-web.js');

const whatsapp             = require("../models/operaciones/whatsapp.model");
const wa                   = require("@open-wa/wa-automate");
const qrcode               = require('qrcode-terminal');
const fs                   = require('fs');
const mime                 = require("mime-types");
const hash                 = require("hash.js");
const moment               = require("moment");
const path                 = require("path");
const { open, writeFile }  = require("fs").promises;


// DATOS PARA ENVIAR LOS MENSAJES POR MEDIO DE UNA API
const axios    = require("axios")
const baseUrl  = "http://127.0.0.1:3006/"


// DATOS PARA LO DE IO
// SOCKETS
const config  = require('../config/index');

let BASE_IMAGE_PATH = path.resolve(__dirname, "..", "..", "whatsapp-imagenes");

// RUTA PARA GUARDAR LAS SESIONES
let BASE_SESION_PATH = path.resolve(__dirname, "..", "..", "sesiones_wha");

const executablePath = '/usr/bin/google-chrome-stable'

console.log('CARGANDO ENCARGADAS INBI')

// https://wharecepinbi.club/kpi/

let sesionesWha = [
  { 
    id              : process.env.ID,
    whatsapp        : process.env.CLIENTE, // 5218126033415
    sucursal        : process.env.SUCURSAL,
    clienteWhatsApp : null
  },
  
  // { 
  //   id              : "encargada-client-two",
  //   whatsapp        : '5218123404920@c.us', // 5218126033415
  //   sucursal        : 'ANAHUAC',
  //   clienteWhatsApp : null
  // },

  // { 
  //   id              : "encargada-client-seis",
  //   whatsapp        : '5218130934877@c.us',
  //   sucursal        : 'FRESNOS',
  //   clienteWhatsApp : null
  // },

  // { 
  //   id              : "encargada-client-siete",
  //   whatsapp        : '5218123405591@c.us',
  //   sucursal        : 'SAN MIGUEL',
  //   clienteWhatsApp : null
  // },

  // { 
  //   id              : "encargada-client-nueve",
  //   whatsapp        : '5218130934874@c.us',
  //   sucursal        : 'PABLO LIVAS',
  //   clienteWhatsApp : null
  // },

  // { 
  //   id              : "encargada-client-cuatro",
  //   whatsapp        : '5218186553151@c.us',
  //   sucursal        : 'CASA BLANCA',
  //   clienteWhatsApp : null
  // },

  // { 
  //   id              : "encargada-client-ocho",
  //   whatsapp        : '5218123404782@c.us',
  //   sucursal        : 'MIGUEL ALEMAN',
  //   clienteWhatsApp : null
  // },

  // { 
  //   id              : "encargada-client-apodaca",
  //   whatsapp        : '5218121911063@c.us',
  //   sucursal        : 'APODACA CENTRO',
  //   clienteWhatsApp : null
  // },

  // { 
  //   id              : "encargada-client-lincoln",
  //   whatsapp        : '5218186581181@c.us',
  //   sucursal        : 'LINCOLN',
  //   clienteWhatsApp : null
  // },

  // { 
  //   id              : "encargada-client-escobedo",
  //   whatsapp        : '5218186581599@c.us',
  //   sucursal        : 'ESCOBEDO',
  //   clienteWhatsApp : null
  // },

  // { 
  //   id              : "encargada-santa-catarina",
  //   whatsapp        : '5218117828258@c.us',
  //   sucursal        : 'SANTA CATARINA',
  //   clienteWhatsApp : null
  // },

  // { 
  //   id              : "encargada-client-monte",
  //   whatsapp        : '5215528477311@c.us',
  //   sucursal        : 'MONTEVIDEO',
  //   clienteWhatsApp : null
  // },
  
]

// Función para validar que exista una sesión
async function existeSesion ( from ){

  const sesion = sesionesWha.find((sesion) => sesion.whatsapp == from);

  if (!sesion) {
    throw new Error('El número de WhatsApp no está registrado. Contacta a sistemas.');
  }

  return sesion;
}

// Función para responder en el socket
async function enviaRespuestaSocket ( canal, cuerpo ){
  try {

    if (config.SOCKET.socketIO) {
      config.SOCKET.socketIO.emit(canal, cuerpo);
    }
    
    return true;

  } catch (error) {

    throw error;
  }
}

// Función para validar que exista una sesión
async function modificarCliente ( from, clienteWhatsApp ){

  const sesion = sesionesWha.find((sesion) => sesion.whatsapp == from);

  if (!sesion) {
    throw new Error('El número de WhatsApp no está registrado. Contacta a sistemas.');
  }

  sesion.clienteWhatsApp = clienteWhatsApp;

  return sesion;
}

const enviarWhatsApp = {

  async getEstatusWhatsApp ( from, SOCKET ) {
    return new Promise(async(resolve, reject) => {
      try{

        // Validar si existe una sesión activa
        const { sucursal, clienteWhatsApp } = await existeSesion( from ).then( response => response )

        // Si hay una sesión... continuamos
        if( clienteWhatsApp ){ 

          const data = await clienteWhatsApp.getState( ).then( response => response )

          if( data == 'CONNECTED'){

            // Enviar respuesta por el socket
            await enviaRespuestaSocket('session-activa', from ).then( response => response )
            
            resolve(data)

          }else{

            // Enviar respuesta por el socket
            await enviaRespuestaSocket('session-inactiva', from ).then( response => response )
            
            resolve(false)

          }
        }else{

          // Enviar respuesta por el socket
          await enviaRespuestaSocket('session-inactiva', from ).then( response => response )
          
          resolve(false)
        }

      }catch( error ){

        const { sucursal, clienteWhatsApp } = await existeSesion( from ).then( response => response )

        if( clienteWhatsApp ){
          clienteWhatsApp.initialize();
        }

        console.log( error )

        reject({ message: 'No existe una sesión creada' } )
      }
    })
  },

  async initiciarSesion ( from ) {
    return new Promise(async(resolve, reject) => {
      try{

        // Validar si existe una sesión activa
        const { id, sucursal, clienteWhatsApp } = await existeSesion( from ).then( response => response )

        let mensaje = 'VERIFICANDO ESTATUS... ' + sucursal 
            
        await enviaRespuestaSocket('estatus-whatsapp', { mensaje: 'VERIFICANDO ESTATUS... ' + sucursal, from } ).then( response => response )

        // Ahora, validamos el estatus de la sesión, para no validar algo que ya existe
        const estatusSesion = await this.getEstatusWhatsApp( from ).then( response => response ) 

        if( !estatusSesion ){

          await enviaRespuestaSocket('estatus-whatsapp', { mensaje: 'INICIANDO... ' + sucursal, from } ).then( response => response )
          
          const client = new Client({
            authStrategy: new LocalAuth({
              clientId: id, 
              // dataPath: sessionDataPath
              // store: store,
              // backupSyncIntervalMs: 600000,
            }),
            puppeteer: { executablePath, args: ['--no-sandbox'] },
          })

          client.on('qr', async ( qr ) => {

            // Generar el QR de la recepcionista y mostrarlo cada que se genere
            qrcode.generate(qr, {small: true});

            const payload = {
              message: `QR GENERADO PARA: ${sucursal}`, 
              qr, 
              from 
            }

            console.log('QR -> ', sucursal)

            // Se envía a alguien 
            resolve( payload )

            await enviaRespuestaSocket('estatus-whatsapp', { mensaje: 'ESCANÉAME ... TIENES 3 MIN.'  + sucursal, from, qr } ).then( response => response )

            await enviaRespuestaSocket('QR-generado', { mensaje: 'ESCANÉAME ... TIENES 3 MIN.'  + sucursal, from, qr } ).then( response => response )

            // Si después de 3 min no se pudo conectar, se elimina ese clien
            setTimeout(async() => {

              // Validar el estatus de la sesión
              const estatusSesion = await this.getEstatusWhatsApp( from ).then( response => response ) 

              // Si la sesion no esta activa....
              if( !estatusSesion ){

                // Destruímos la sesión
                client.destroy();
                
                // Eliminamos el cliente
                await modificarCliente( from, null ).then( response => response )

                await enviaRespuestaSocket('estatus-whatsapp', { mensaje: 'FALLÓ AUTENTICACIÓN, FAVOR DE INICIALIAR NUEVAMENTE ... '  + sucursal, from, qr, estatus: 2 } ).then( response => response )

              }
            }, 180000);

          });

          // Cargando vistas
          client.on('loading_screen', async (percent, message) => {

            console.log('Cargando vista ', percent, ' ',sucursal)
            
            await enviaRespuestaSocket('estatus-whatsapp', { mensaje: 'Cargando mensajes...' + percent  + sucursal, from, estatus: null } ).then( response => response )

          });   

          // Save session values to the file upon successful auth
          client.on('authenticated', async (session) => {

            console.log(`sesion conectada: ${ sucursal }`)

            await enviaRespuestaSocket('session-activa', from ).then( response => response )

            await enviaRespuestaSocket('estatus-whatsapp', { mensaje: 'Conectado, guardando la sesión, espere....', from, estatus: 1 } ).then( response => response )

          });

          // LA AUTENTICACIÓN FALLÓ
          client.on('auth_failure', async(error) => {

            if (error instanceof AuthenticationFailureError) {
              console.log('Error en la autenticación:', error.message);
            } else {
              console.log('Ocurrió un error durante la autenticación:', error);
            }

            // Validar el estatus de la sesión
            const estatusSesion = await this.getEstatusWhatsApp( from ).then( response => response ) 

            if( !estatusSesion ){
              
              // Destruímos la sesión
              client.destroy();
              
              // Eliminamos el cliente
              await modificarCliente( from, null ).then( response => response )

              await enviaRespuestaSocket('estatus-whatsapp', { mensaje: 'FALLÓ AUTENTICACIÓN, FAVOR DE INICIALIAR NUEVAMENTE ... '  + sucursal, from, qr, estatus: 2 } ).then( response => response )

            }
          });

          // lA SESIÓN YA ESTÁ LISTA PARA USARSE
          client.on('ready', async( session ) => {

            console.log('LA SESIÓN ESTÁ LISTA: ', sucursal );

            await enviaRespuestaSocket('session-activa', from ).then( response => response )

            await enviaRespuestaSocket('estatus-whatsapp', { mensaje: 'Conectado', from, estatus: 1 } ).then( response => response )

          });

          client.on('disconnected', async ( reason ) => {
            try{

              console.log('desconectada', reason )

              const { whatsapp, id } = await existeSesion( from ).then( response => response )
              
              if( reason == 'NAVIGATION' ){
                
                await modificarCliente( whatsapp, null ).then( response => response )

                this.initiciarSesion( whatsapp )

              }else{

                setTimeout(() => {
                  client.initialize();
                }, 5000); // Esperar 5 segundos antes de reconectar
              
              }
              
              await enviaRespuestaSocket('estatus-whatsapp', { mensaje: 'SESIÓN ELIMINADA DEL TELÉFONO... ', from: whatsapp, estatus: 2 } ).then( response => response )


            }catch( error ){
              console.log( 'error',error)
            }
          });

          client.on('remote_session_saved', async( session ) => {
            
            console.log('SESION GUARDADA', sucursal )

            await enviaRespuestaSocket('session-activa', from ).then( response => response )
            
            await enviaRespuestaSocket('estatus-whatsapp', { mensaje: 'Sesión guardada correctamente', from, estatus: 1 } ).then( response => response )
            
          }) 

          // ESCUCHAR CUANDO SE GENERÉ UN MENSAJE Y SE GUARDA
          client.on('message_create', async ( message_create ) => {
            try {

              let permisos = ['chat', 'video', 'image', 'ptt', 'document' ]
              
              // VALIDAR SI ES NECESARIO GUARDAR ESTOS DATOS
              if( message_create._data.id.remote != 'status@broadcast' && permisos.includes( message_create._data.type )){

                let fullFileName = ''

                if( message_create._data.mimetype ){
                  const fileName     = hash.sha256().update(moment.now().toString()).digest("hex");
                  const extension    = mime.extension(message_create._data.mimetype);
                  fullFileName       = `${fileName}.${extension}`;
                  const fullPath     = path.join(
                    path.relative(__dirname, BASE_IMAGE_PATH),
                    fullFileName
                  );
                  const mediaData = await wa.decryptMedia(message_create._data);
                  try {
                    await writeFile(fullPath, mediaData);
                  } catch (error) {
                    console.log(error, 'here 3');
                  }
                }

                try {

                  // EMITIR QUE HAY MENSAJES NUEVOS
                  const payload = {
                    fromMe  : message_create._data.id.fromMe,
                    froms   : message_create.from, 
                    message : message_create
                  }

                  config.SOCKET.sendMessage( payload )
      
                  const response = await axios.post(baseUrl + "whatsapp.savemessage", {
                    message_create,
                    fullFileName
                  }).then(response => {
                    console.log('Mensaje creado', sucursal)
                  }).catch((err) => {
                    console.log('Mensaje NOOOOO guardado', sucursal)
                  });


                } catch (error) {
                  return res.status(500).send({ message: error ? error.message : 'Error en el servidor'})
                }

              }

            } catch (error) {
              console.log( 'ERROR EN LA BASE DE DATOS' )
            }
          });

          await modificarCliente( from, client ).then( response => response )
          
          client.initialize();
        }else{
          console.log('Sesión activa, no ocupa inicia')
        }
        
      }catch( error ){
        reject({ message: 'No existe una sesión creada', error } )
      }
    })
  },
  
  async enviarMensajeInbi ( tos, mensaje, imagen, outputFilename, from ) {
    return new Promise(async(resolve, reject) => {
      try{

        // Validar si existe una sesión activa
        const { id, sucursal, clienteWhatsApp } = await existeSesion( from ).then( response => response )

        const number_details = await clienteWhatsApp.getNumberId( tos ).then( response => response )

        if (number_details && number_details._serialized ) {

          const fullPath     = path.join(
            path.relative(__dirname, BASE_IMAGE_PATH),
            outputFilename
          );

          const media = MessageMedia.fromFilePath(fullPath);

          let sendMessageData = null

          if(imagen){
            sendMessageData = await clienteWhatsApp.sendMessage( number_details._serialized, media, { caption: "Buen día, le envío su recibo de pago, espero tenga un excelente día" }); // send message
            sendMessageData = await clienteWhatsApp.sendMessage( number_details._serialized, 'Buen día, le envío su recibo de pago, espero tenga un excelente día' ); // send message
          }else{
            sendMessageData = await clienteWhatsApp.sendMessage( number_details._serialized, mensaje ); // send message
          }
          console.log( 'sendMessageData',sendMessageData )
          resolve( sendMessageData )
        } else {
          console.log( 'final_number', number_details )

          reject({ message: tos + "Teléfono no existe"})
        }

      }catch( error ){
        console.log( 'error',error )
        reject({ message: 'Espera 15 segundos, y vuelve a intentarlo, por favor' })
      }
    })
  },

  async destroySesion ( from ) {
    return new Promise(async(resolve, reject) => {
      try{

        const { id, sucursal, clienteWhatsApp } = await existeSesion( from ).then( response => response )

        if( clienteWhatsApp ){ 

          clienteWhatsApp.destroy();

        }
        
        await modificarCliente( from,  null ).then( response => response )
        
        resolve({ message: 'Sesión cerrada, favor de activarla de nuevo' })

      }catch( error ){
        console.log( error )
        reject({ message: 'No existe una sesión creada', error } )
      }
    })
  },

  async whatChats ( from ) {
    return new Promise(async(resolve, reject) => {
      try{

        // Validar si existe una sesión activa
        const { id, sucursal, clienteWhatsApp } = await existeSesion( from ).then( response => response )

        // Si hay una sesión... continuamos
        if( clienteWhatsApp ){ 

          const data = await clienteWhatsApp.getState( ).then( response => response )

          console.log( 'whatChat',data )

          if( data == 'CONNECTED'){

            let chat_activos = await clienteWhatsApp.getChats( )

            chat_activos = chat_activos.filter( el => { return !el.isGroup })

            for (let chat of chat_activos){
              if( chat.unreadCount > 0){
                unreadMessages = await chat.fetchMessages( { limit: chat.unreadCount } );
                chat['no_leidos'] = unreadMessages
              }else{
                chat['no_leidos'] = []
              }
              chat['ultimo_mensaje'] = await chat.fetchMessages( { limit: 1 } );
            }

            console.log( 'regresar mensajes ')


            resolve( chat_activos )

          }else{


            await enviaRespuestaSocket('session-inactiva', from ).then( response => response )

            console.log( 'Sesión inactiva')

            return reject({ message: 'Favor de iniciar sesión en la plataforma' } ) 

          }
        }else{

          await enviaRespuestaSocket('session-inactiva', from ).then( response => response )

          console.log( 'Sesión inactiva')

          return reject({ message: 'Favor de iniciar sesión en la plataforma' } ) 

        }


      }catch( error ){
        reject({ message: 'Sesión creada, pero no se pudieron recuperar mensajes', error } )
      }
    })
  },

  async enviarMensaje ( froms, _serialized, mensaje, imagen, outputFilename ){
    return new Promise(async(resolve, reject) => {
      try{

        // Validar que la sesión existea
        let existeSesion = sesionesWha.find( el => el.whatsapp == froms )

        // Si no existe una sesión retornarlo
        if( !existeSesion ){ return reject({ message: 'Teléfono no configurado' }) }

        // Sacar la sesión de cliente de whatsApp
        const { clienteWhatsApp } = existeSesion 

        // Validar que el número si exista
        if ( _serialized ) {

          let sendMessageData = null

          const specificChat = await clienteWhatsApp.getChatById( _serialized );
          await specificChat.sendSeen();

          if( parseInt( imagen ) == 1 ){

            const fullPath     = path.join(
              path.relative(__dirname, BASE_IMAGE_PATH),
              outputFilename
            );

            const media = MessageMedia.fromFilePath(fullPath);
            sendMessageData = await clienteWhatsApp.sendMessage( _serialized, media, { caption: mensaje }); // send message

          }else{
            sendMessageData = await clienteWhatsApp.sendMessage( _serialized, mensaje ); // send message
          }

          resolve( sendMessageData )
        } else {
          console.log( 'final_number', _serialized )
          reject({ message: tos + "Teléfono no existe"})
        }

      }catch( error ){
        console.log( 'error',error )
        reject({ message: 'Espera 15 segundos, y vuelve a intentarlo, por favor' })
      }
    })
  },
}

// for( const i in sesionesWha ){
//   const { id, sucursal, clienteWhatsApp, whatsapp } = sesionesWha[i]
//   enviarWhatsApp.initiciarSesion( whatsapp )
// }

module.exports = enviarWhatsApp;