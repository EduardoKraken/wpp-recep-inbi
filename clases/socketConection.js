const config         = require('../config/index');

class Sockets {

	constructor( ){
		this.usuarios       = []
		this.socketIO       = null
		this.sesiones       = []
    this.client         = null
	}

  //* Funcion inicial de conexion de los socket con el servicio.
  generarConexion( server ) {
    var io = require("socket.io")( server, { path: `/${process.env.RUTASOCKET}/socket.io` });

		io.on('connection', ( client ) => {

      console.log('Conectado al socket')

			// GUARDAMOS TODO EN EL CONFIG
			config.IO     	= io
			config.SERVER 	= server

			this.socketIO   = io
      this.client     = client

		  client.on('nuevaAsignacion', async ( ) => {

		    try{ 

		      // Responderse a si mismos
		      client.broadcast.emit('nuevaAsignacion')
		    
		    }catch( error ){

		      console.log( error )
		    }

		  })

		  client.on('iniciarSesionWha', async ( usuario ) => {

		  	// Verificamos si existe un usuario
		  	const existeUsuario = this.usuarios.find( el => el.iderp == usuario.iderp )

		  	// Si lo hay... hay que eliminarlo
		  	if( existeUsuario )
		  		this.usuarios = this.usuarios.filter( el => el.iderp != usuario.iderp )

		  	// Agragar a la persona
		  	this.usuarios.push({ id: client.id, iderp: usuario.iderp, whatsappservidor: usuario.whatsappservidor })

		  })

		  client.on('disconnect', () => {

		    this.usuarios = this.usuarios.filter( el => el.id != client.id )

		  })

		});
  };

  getContacts( from ){
    return new Promise(async(resolve, reject) => {
      try{ 

        // Consultar chats del teléfono
        const chats  = await config.WHATSAPP.whatChats( from )

        resolve( chats )
      
      }catch( error ){

        // Retornarmos algun error que haya salido 
        reject( error )
      }
    })
  };

  async createQR( from ){
    try{ 

    	// Consultar telefono del alumno
    	const data     = await config.WHATSAPP.generaQR( from, true, config.IO )

    }catch( error ){

    	// Retornarmos algun error que haya salido 
      console.log( error )
    }

  };

  async verEstatus ( from ){
    return new Promise(async(resolve, reject) => {
      try{ 

        // Validar el estatus de la sesión
        const data  = await config.WHATSAPP.getEstatusWhatsApp( from, config.IO ).then( response => response )

        resolve( data )

      }catch( error ){
        console.log( error )
        // Retornarmos algun error que haya salido 
        reject( error )
      }
    })
  };

  async inicializarWhatsApp( from ){
    return new Promise(async(resolve, reject) => {
      try{ 

        // Consultar telefono del alumno
        const data     = await config.WHATSAPP.initiciarSesion( from ).then( response => response )
        
        resolve( data )

      }catch( error ){

        // Retornarmos algun error que haya salido 
        reject( error )
      }
    })
  };

  async destroySesion( from ){
    return new Promise(async(resolve, reject) => {
      try{ 

        // Consultar telefono del alumno
        const data     = await config.WHATSAPP.destroySesion( from ).then( response => response )
        
        resolve( data )

      }catch( error ){

        // Retornarmos algun error que haya salido 
        reject( error )
      }
    })
  };

  async sendMessage( message ){
    try{ 

      // Responderse a si mismos
      config.IO.emit('mensajesCreados', message )
    
    }catch( error ){

    	// Retornarmos algun error que haya salido 
      console.log( error )
    }

  };

  sessionActiva( payload ){

  	// Varificamos si ya esta dada de alta la sesion
  	const existeSesion = this.sesiones.find( el => el.whatsapp == payload.whatsapp )

  	if( !existeSesion ){
  		
  		// Si no existe la sesión, la agregamos y listo
  		this.sesiones.push( payload )
  	
  	}

    return this.sesiones

  };


  async sessionInactiva( payload ){
    try{ 

    	// Varificamos si ya esta dada de alta la sesion
    	this.sesiones = this.sesiones.filter( el => el.whatsapp != payload.whatsapp )

    	console.log( 'sesiones activas' ,this.sesiones )
    
    }catch( error ){

    	// Retornarmos algun error que haya salido 
      console.log( error )
    }

  };

  
  getSessiones( ){

    let usuarios = this.sesiones  
    return usuarios

  }


  getSession( wha ){

    let existeUsuarioSocket = this.usuarios.find( el => el.whatsappservidor == wha )

    let existeSession       = this.sesiones.find( el => el.whatsapp == wha )

    return existeUsuarioSocket && existeSession ? existeUsuarioSocket : false
  }
}

module.exports = { Sockets };