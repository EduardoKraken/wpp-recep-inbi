
class Usuarios{

	constructor( ){
		this.personas = []
	}

	agregarPersona( id, nombre, sucursal, iderp ){
		
		let persona = { id, nombre, sucursal, iderp }

		this.personas.push( persona )

		return this.personas

	}


	getPersona( id ){

		return this.personas.filter( persona => persona.id == id )[0]

	}

	getPersonas( ){

		return this.personas 
	
	}

	getPersonaSala( sala ){
		// ...
	}


	eliminarPersona( id ){

		let personaEliminada = this.getPersona( id )

		this.personas = this.personas.filter( persona => persona.id != id )

		return personaEliminada

	}


}

module.exports = {
	Usuarios
}