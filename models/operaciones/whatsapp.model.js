const { result }  = require("lodash");
// const sqlERPNUEVO = require("../db.js");
// const sqlERP      = require("../db3.js");
// const sqlFAST     = require("../dbFAST.js");
// const sqlINBI     = require("../dbINBI.js");

//const constructor
const whatsapp = function(depas) {};

whatsapp.agregarMensajeWhatsApp = ( message_create, fullFileName ) => {

  var u = {
    fromMe                      : message_create._data.id.fromMe,
    remote                      : message_create._data.id.remote,
    mimetype                    : message_create._data.mimetype ? message_create._data.mimetype : '',
    filename                    : message_create._data.filename ? message_create._data.filename : '',
    filehash                    : message_create._data.filehash ? message_create._data.filehash : '',
    body                        : message_create._data.body,
    type                        : message_create._data.type,
    timestamp                   : message_create._data.t,
    notifyName                  : message_create._data.notifyName,
    lastPlaybackProgress        : message_create._data.lastPlaybackProgress,
    isMdHistoryMsg              : message_create._data.isMdHistoryMsg,
    stickerSentTs               : message_create._data.stickerSentTs,
    isAvatar                    : message_create._data.isAvatar,
    requiresDirectConnection    : message_create._data.requiresDirectConnection,
    pttForwardedFeaturesEnabled : message_create._data.pttForwardedFeaturesEnabled,
    isStatusV3                  : message_create._data.isStatusV3,
    mediaKey                    : message_create.mediaKey,
    hasMedia                    : message_create.hasMedia,
    from                        : message_create.from,
    to                          : message_create.to,
    author                      : message_create.author,
    deviceType                  : message_create.deviceType,
    hasQuotedMsg                : message_create.hasQuotedMsg,
    duration                    : message_create.duration,
    location                    : message_create.location,
    isGif                       : message_create.isGif,
    isEphemeral                 : message_create.isEphemeral,
    fullFileName
  }

  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query('INSERT INTO mensajes_whatsapp (`fromMe`, `remote`, `body`, `type`, `timestamp`, `notifyName`, `isMdHistoryMsg`, `stickerSentTs`, `isAvatar`, `requiresDirectConnection`, `pttForwardedFeaturesEnabled`, `isEphemeral`, `isStatusV3`, `mediaKey`, `hasMedia`, `froms`, `tos`, `author`, `deviceType`, `hasQuotedMsg`, `duration`, `location`, `isGif`, `mimetype`, `filename`, `filehash`, `fullFileName` )VALUES( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )', 
    	[ u.fromMe,u.remote,u.body,u.type,u.timestamp,u.notifyName,u.isMdHistoryMsg,u.stickerSentTs,u.isAvatar,u.requiresDirectConnection,u.pttForwardedFeaturesEnabled,u.isEphemeral,u.isStatusV3,u.mediaKey,u.hasMedia,u.from,u.to,u.author,u.deviceType,u.hasQuotedMsg,u.duration,u.location,u.isGif, u.mimetype, u.filename, u.filehash, u.fullFileName ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage, err }); }
      resolve({ id: res.insertId, ...u })
    })
  })
}


whatsapp.guardarInfoReciboPago = ( id_alumno, id_grupo, id_usuario ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query('INSERT INTO recibos_enviados ( id_alumno, id_grupo, id_usuario )VALUES( ?,?,?)', [ id_alumno, id_grupo, id_usuario ],
      (err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id_alumno, id_grupo, id_usuario })
    })
  })
}

whatsapp.updateTelefonoERP = ( id_alumno, telefono ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`UPDATE alumnos SET telefono = ? WHERE id_alumno = ?;`,[ telefono, id_alumno ], (err, res) => {
      if (err) {  return reject({ message: err.sqlMessage + ' -> actualizar telefono del erp viejo' + ' -> ' + err }); }
      if (res.affectedRows == 0) {
        return reject({ message: 'No existe el datos para actualizar' });
      }
      resolve(res)
    });
  });
};

whatsapp.updateTelefonoLMS = ( id_alumno, tos, escuela ) => {
  const db = escuela == 2 ? sqlFAST : sqlINBI
  return new Promise((resolve,reject)=>{
    db.query(`UPDATE usuarios SET whatsapp = ? WHERE id > 0 AND iderp = ?;`,[ tos, id_alumno ], (err, res) => {
      if (err) {  return reject({ message: err.sqlMessage + ' -> updateTelefonoLMS' }); }
      resolve(res)
    });
  });
};

whatsapp.existeAlertaDesconeta = (  froms ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM sesiones_whatsapp WHERE whatsapp = ? AND deleted = 0;`,[ froms ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

whatsapp.addAlertaDesconecta = (  froms ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query('INSERT INTO sesiones_whatsapp ( whatsapp ) VALUES ( ? )', [ froms ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve({ id: res.insertId, froms })
    })
  })
}

whatsapp.updateAlertaWha = ( from ) => {
  return new Promise((resolve, reject) => {
    sqlERPNUEVO.query(`UPDATE sesiones_whatsapp SET deleted = 1, notificacion = 1  WHERE idsesiones_whatsapp > 0 AND whatsapp = ?;`, [ from ],(err, res) => {
      if (err) { reject(err); return; }
      resolve({ from });
    });
  });
};

whatsapp.usuariosWhatsApp = (  ) => {
  return new Promise((resolve,reject)=>{
    sqlERP.query(`SELECT u.id_usuario, CONCAT(UPPER(u.nombre_completo),' / ', p.plantel) AS nombre_completo, u.whatsappservidor, u.url_servidor, p.plantel,
      IF(LOCATE('FAST', p.plantel)>0, 2, 1) AS escuela FROM usuarios u
      LEFT JOIN planteles p ON p.id_plantel = u.id_plantel_oficial
      WHERE u.whatsappservidor ORDER BY nombre_completo;`,(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res)
    })
  })
}

whatsapp.getAsignacion = ( id_whatsapp ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT * FROM acceso_whats WHERE id_whatsapp = ? AND deleted = 0; `,[ id_whatsapp ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve(res[0])
    })
  })
}

whatsapp.getMensajesIdWhat = (  idWha, telefonos ) => {
  return new Promise((resolve,reject)=>{
    sqlERPNUEVO.query(`SELECT *, IF(LOCATE('${idWha}',froms)>0,1,0) AS mio FROM mensajes_whatsapp 
      WHERE froms LIKE '%${idWha}%' ${ idWha.match( '@c.us' ) ? ` AND remote NOT LIKE "%@g.us%" `  : ` ` }
      AND tos IN ( ? ) AND remote NOT LIKE "%@g.us%"
      OR tos LIKE '%${idWha}%' ${ idWha.match( '@c.us' ) ? ` AND remote NOT LIKE "%@g.us%" ` : ` ` } 
      AND froms IN ( ? ) AND remote NOT LIKE "%@g.us%"
      ORDER BY idmensajes_whatsapp DESC
      LIMIT 100`,[ telefonos, telefonos ],(err, res) => {
      if(err){ return reject({ message: err.sqlMessage }); }
      resolve(res)
    })
  })
}

module.exports = whatsapp;

