module.exports = app => {
  const whatsapp = require('../../controllers/whatsapp/whatsapp.controllers') // --> ADDED THIS


  app.post("/whatsapp.activo",           whatsapp.verEstatusWhatsApp);
  app.post("/whatsapp.inicializar",      whatsapp.inicializarWhatsApp); 
  app.post("/whatsapp.destruir",         whatsapp.destroySesion); 
  app.post("/whatsapp.chats",            whatsapp.whatChats); 
   
  // app.post("/whatsapp.recibo",           whatsapp.generarReciboWhatsApp); 
  // app.post("/whatsapp.enviarrecibo",     whatsapp.enviarReciboErp); 

  app.post("/whatsapp.mensaje",          whatsapp.enviarMensajeWpp); 
  app.post("/audio.parse",               whatsapp.parseAudio);
  app.post("/whatsapp.chat.telefono",    whatsapp.buscarWhatsAppTelefono);



};